\section{Les principes de la conception orientée objets}

\subsection{Introduction}
Pour arriver à notre but de créer un programme maintenable, flexible, et réutilisable,
nous avons besoin de principes qui vont ne permettre de déterminer si une partie de 
notre diagramme de classe satisfait ses trois besoins.

Les principes de la conception orientée objets, ont été formulé pour la première fois en 1995 par
Robert Martin. Ces onze principes visent à simplifier la minimisation des dépendances entre
les différents composants d'un système orienté objets. Il sont classés en 3 catégories:

\begin{itemize}
    \item \textbf{Principes de conception des classes:} \\
    Parfois appelés principes \textbf{\texttt{SOLID}}, ce sont des principes à respecter
    lors de la mise en œuvre d'un diagramme de classes.
    
    \begin{table}[H]
        \begin{tabular}{|p{3cm}|p{\textwidth - 3cm}|}
            \hline
            SRP & The Single Responsibility Principle  \\
            OCP & The Open Close Principle             \\
            LSP & The Liskov Substitution Principle    \\
            ISP & The Interface Segregation Principle  \\
            DIP & The Dependency Inversion Principle   \\
            \hline
        \end{tabular}
        \caption{ Les principes SOLID}
    \end{table}
    
    \item \textbf{Principes de la cohésion des packages:} \\
    Dans ce contexte \emph{package} veut dire un livrable binaire comme un fichier \verb!.jar! ou \verb!.dll!    
    et pas un namespace en C++ ou package en Java. Les principes de cohésion indique quelles classes
    doivent être dans le même package.
    
    \begin{table}[H]
        \begin{tabular}{|p{3cm}|p{\textwidth - 3cm}|}
            \hline
            REP & The Release Reuse Equivalency Principle \\
            CCP & The Common Closure Principle \\
            CRP & The Common Reuse Principle \\
            \hline
        \end{tabular}
        \caption{Principes de la cohésion des packages}
    \end{table}
    
    \item \textbf{Principes du couplage entre packages:} \\
    Concernent les relations de dépendances entres les packages.
    
    \begin{table}[H]
        \begin{tabular}{|p{3cm}|p{\textwidth - 3cm}|}
            \hline
            ADP & The Acyclic Dependencies Principle \\
            SDP & The Stable Dependencies Principle \\
            SAP & The Stable Abstractions Principle \\
            \hline
        \end{tabular}
        \caption{Principes du couplage entre packages}
    \end{table}
    
\end{itemize}

Étant donné que notre application est composée d'un seul fichier exécutable, nous nous sommes
intéressés qu'aux principes relatives à la conception des classes. La partie suivante présente
une explication de ses 5 principes.

\subsection{Les principes SOLID}

\subsubsection{SRP}
\begin{quote}
        ``\emph{A class should have one, and only one, reason to change.}''
\end{quote}

Le SRP est le principe de responsabilité unique indique
qu'une classe doit avoir une seule raison de changer. Car donner plusieurs responsabilités à 
une seul classe diminue la modularité, et augmente la possibilité d'introduire des bugs lors
de la modification. Par exemple une classe qui fait la modification des données et leurs
présentation viole le principe SRP.
     
\subsubsection{OCP}
\begin{quote}
``\emph{You should be able to extend a classe's behavior, without modifying it.}''
\end{quote}

Le principe OCP ou le principe Ouvert/Fermé, indique qu'une classe doit être fermée pour la 
modification mais ouverte pour l'extension. C'est-à-dire qu'elle doit permettre l'ajout de 
nouvelle fonctionnalité sans modifier son code source.

L'application de principe se fait généralement par l'utilisation du l'héritage et du polymorphisme.
considérons le code suivant:

\begin{lstlisting}[language=C++]
void Dessinateur::dessiner(string forme) {
   if(forme == "cercle")
        dessinerCercle();
    else if( forme == "ligne")
        dessinerLigne();        
}
    \end{lstlisting}
    
Cette méthode fait le dessin d'une forme selon le type donné en argument. Si on a besoin
d'ajouter un nouveau type nous devrons modifier son code. On peut appliquer l'OCP ici grâce au
polymorphisme en définissant une interface
\texttt{IForme} qui contient une méthode \texttt{dessiner()} et qu'on implémente à chaque fois qu'on a
besoin d'ajouter un type de forme.

\begin{lstlisting}[language=C++]
void Dessinateur::dessiner(IForme *forme) {
    forme->dessiner();
}
\end{lstlisting}
    
\subsubsection{LSP}
\begin{quote}
    ``\emph{Derived classes must be substitutable for their base classes.}''
\end{quote}

Le LSP ou principe de substitution de Liskov, concerne l'utilisation de l'héritage.
Il indique qu'une sous classe doit être utilisable à la place de sa classe mère dans n'importe
quelle situation. si ce principe n'est pas appliqué dans notre cas nous devons opter pour une
autre manière de conception, en utilisant par exemple la composition.

La figure \ref{lsp_error} montre un exemple d'une relation d'héritage qui ne respecte pas le
principe LSP. Mathématiquement un carré est un rectangle, mais en essayant d'appliquer cette 
règle en programmation orientée objets, nous avons donner la possibilité au programmeur 
d'affecter deux valeurs différentes à la hauteur et à la largeur d'un carré.
\begin{figure}
    \begin{center}
        \includegraphics[scale=0.8]{images/lsp_error.eps}
    \end{center}
    \caption{\label{lsp_error}Exemple de violation du principe LSP}
\end{figure}

\subsubsection{ISP}
\begin{quote}
    ``\emph{Make fine grained interfaces that are client specific.}''
\end{quote}
    
Le principe ISP (Principe d'Inversion des Dépendances) ressemble beaucoup au principe SRP 
dans le fait qu'il minimise les responsabilités attribuées, mais pour les interfaces au lieu des classes.
    
Le but de l'ISP est d'éliminer les interfaces ayant un grand nombre de méthodes. Car elles peuvent
obliger les classes qui les implémentent à dépendre de méthodes dont elles n'ont pas besoin. Donc pour
appliquer le principe ISP il faut diviser l'interface en plusieurs sous-interfaces plus spécialisées.

\subsubsection{DIP}
\begin{quote}
    ``\emph{Depend on abstractions, not on concretions.}''
\end{quote}

Le principe DIP, ou principe d'inversion des dépendances, indique qu'une classe doit dépendre d'abstractions
et pas de composition. Selon ce principe un système dans lequel il y a des classes de haut niveau et des
classes de bas niveaux, les classes de haut niveau ne doivent pas dépendre directement des classes de bas
niveau. Mais elles doivent dépendre d'une interface abstraite qui fait l'intermédiaire entre les deux,
pour minimiser l'impact à la classe de bas niveau.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.8]{images/dip_example.eps}
    \end{center}
    \caption{Application du principe DIP}
\end{figure}

